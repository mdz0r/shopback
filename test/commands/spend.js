'use strict'
/* global describe, before, it */

require('chai').should()

describe('command/spend.js', function () {
  describe('.handler', function () {
    // Mocks
    let _latestLogMessage
    const log = { debug: function (msg) { _latestLogMessage = msg } }

    // Target
    const _module = require('../../lib/commands/spend')
    let _target

    before(function () {
      _latestLogMessage = null
      _target = _module({ log })
    })

    it('positive case allAmountsOver50', function () {
      _target.handler({ amounts: [50, 100] })
      _latestLogMessage.should.equal('Award cashback: 20.00')
    })
    it('positive case allAmountsOver20', function () {
        _target.handler({ amounts: [20, 20, 20] })
        _latestLogMessage.should.equal('Award cashback: 3.00')
    })
    it('positive case allAmountsOver10', function () {
        _target.handler({ amounts: [10, 15, 10, 15] })
        _latestLogMessage.should.equal('Award cashback: 1.50')
    })
    it('positive case allAmountsOver10', function () {
        _target.handler({ amounts: [10, 10, 10] })
        _latestLogMessage.should.equal('Award cashback: 1.00')
    })
    it('positive case AmountsUnder10', function () {
        _target.handler({ amounts: [5, 5] })
        _latestLogMessage.should.equal('Award cashback: 0.25')
    })
    it('negative case empty amounts', function () {
        _target.handler({ amounts: [] })
        _latestLogMessage.should.equal('No cashback')
    })
    it('negative case negative amounts', function () {
        _target.handler({ amounts: [-10, -50] })
        _latestLogMessage.should.equal('No cashback')
    })
  })
})