'use strict'
/* global describe, before, it */

require('chai').should()

describe('command/redeem.js', function () {
  describe('.handler', function () {
    // Mocks
    let _latestLogMessage
    const log = { debug: function (msg) { _latestLogMessage = msg } }

    // Target
    const _module = require('../../lib/commands/redeem')
    let _target

    before(function () {
      _latestLogMessage = null
      _target = _module({ log })
    })

    it('positive case www.shopback.sg', function () {
      _target.handler({ domain: 'www.shopback.sg' })
      _latestLogMessage.should.equal('Visit https://www.shopback.sg to start earning cashback!')
    })
    it('positive case www.shopback.com.tw', function () {
        _target.handler({ domain: 'www.shopback.com.tw' })
        _latestLogMessage.should.equal('Visit https://www.shopback.com.tw to start earning cashback!')
    })
    it('negative case unrecognized domain', function () {
        _target.handler({ domain: 'www.shopback.fr' })
        _latestLogMessage.should.equal('unrecognized domain')
    })
    it('negative case empty domain', function () {
        _target.handler({ domain: '' })
        _latestLogMessage.should.equal('unrecognized domain')
    })
    it('negative case int domain', function () {
        _target.handler({ domain: 1 })
        _latestLogMessage.should.equal('unrecognized domain')
    })
  })
})