'use strict'
/* global describe, before, it */

require('chai').should()

describe('command/signup.js', function () {
  describe('.handler', function () {
    // Mocks
    let _latestLogMessage
    const log = { debug: function (msg) { _latestLogMessage = msg } }

    // Target
    const _module = require('../../lib/commands/signup')
    let _target

    before(function () {
      _latestLogMessage = null
      _target = _module({ log })
    })

    it('positive case www.shopback.sg', function () {
      _target.handler({ domain: 'www.shopback.sg' })
      _latestLogMessage.should.equal('Award bonus: 5.00 SGD')
    })
    it('positive case www.shopback.co.id', function () {
        _target.handler({ domain: 'www.shopback.co.id' })
        _latestLogMessage.should.equal('Award bonus: 25.00 IDR')
    })
    it('negative case unrecognized domain', function () {
        _target.handler({ domain: 'www.shopback.fr' })
        _latestLogMessage.should.equal('unrecognized domain')
    })
    it('negative case empty domain', function () {
        _target.handler({ domain: '' })
        _latestLogMessage.should.equal('unrecognized domain')
    })
    it('negative case int domain', function () {
        _target.handler({ domain: 1 })
        _latestLogMessage.should.equal('unrecognized domain')
    })
  })
})