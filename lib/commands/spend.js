'use strict'

module.exports = function (dep) {
  let cmd = {}
  cmd.command = 'spend <amounts...>'
  cmd.desc = 'Return cashback awarded rounded to 2 decimal places.'
  cmd.builder = {}
  cmd.handler = function (argv) {
    const { log } = dep
    const amounts = argv['amounts']

    var highestAmount = 0
    var allAmountsOver50 = true, allAmountsOver20 = true, allAmountsOver10 = true

    for (var i = 0; i < amounts.length; i++) {
      if (amounts[i] > highestAmount) {
        highestAmount = amounts[i]
      }
      if (amounts[i] < 10) {
        allAmountsOver10 = false, allAmountsOver20 = false, allAmountsOver50 = false
      } else if (amounts[i] < 20) {
        allAmountsOver20 = false, allAmountsOver50 = false
      } else if (amounts[i] < 50) {
        allAmountsOver50 = false
      }
    }

    if (highestAmount <= 0) {
      log.debug("No cashback")
      return      
    }

    if (allAmountsOver50) {
      log.debug("Award cashback: " + Number(Math.round(highestAmount*0.20+'e2')+'e-2').toFixed(2))
      return
    } else if (allAmountsOver20) {
      log.debug("Award cashback: " + Number(Math.round(highestAmount*0.15+'e2')+'e-2').toFixed(2))
      return
    } else if (allAmountsOver10) {
      log.debug("Award cashback: " + Number(Math.round(highestAmount*0.10+'e2')+'e-2').toFixed(2))
      return
    }

    log.debug("Award cashback: " + Number(Math.round(highestAmount*0.05+'e2')+'e-2').toFixed(2))
  }
  return cmd
}