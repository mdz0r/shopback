'use strict'

module.exports = function (dep) {
  let cmd = {}
  cmd.command = 'signup <domain>'
  cmd.desc = 'Award corresponding bonus amount with currency for each of'
  cmd.builder = {}
  cmd.handler = function (argv) {
    const { log } = dep
    const { domain } = argv

    var domains = []
    domains["www.shopback.sg"] = ["SGD", 5]
    domains["www.shopback.my"] = ["MYR", 10]
    domains["www.shopback.co.id"] = ["IDR", 25.000]
    domains["www.shopback.com.tw"] = ["TWD", 1000]
    domains["www.myshopback.co.th"] = ["THB", 500]
    domains["www.shopback.com"] = ["USD", 5]

    if (!domains[domain]) {
        log.debug("unrecognized domain")
        return
    }
    log.debug("Award bonus: " + Number(Math.round(domains[domain][1]+'e2')+'e-2').toFixed(2) + " " + domains[domain][0])
  }
  return cmd
}