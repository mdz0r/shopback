'use strict'

module.exports = function (dep) {
  let result = {}

  result.debug = function (...msg) {
    const { console } = dep
    console.log(msg.join(" "))
  }

  return result
}